let Calculator = require('../modules/Calculator.js')
let cal = new Calculator();

describe("describe is a suite which is just like a function", function(){

    //it is like a test and is so called as spec
    it("tests add functionality", function(){
        expect(cal.Add(10,20)).toBe(30)
    })

    it("tests sub functionality", function(){
        expect(cal.Sub(20,10)).toBe(10)
    })

    it("tests multiply functionality", function(){
        expect(cal.Multiply(20,30)).toBe(600)
    })

    it("tests divide functionality", function(){
        expect(cal.Divide(20,30)).toBe(50)
    })

})